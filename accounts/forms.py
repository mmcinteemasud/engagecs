from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from django.db import transaction
from .models import Teacher, Student, Profile
from classes.models import Classroom
from django.contrib.auth.forms import UserCreationForm
from units.models import Unit, Grade, Subject, School
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth import get_user_model
from PIL import Image
from django.core.files import File
from django.core.files.storage import default_storage as storage
from io import BytesIO
User = get_user_model()

class TeacherRegisterForm(UserCreationForm):
	school = forms.ModelChoiceField(queryset=School.objects.all(), widget=forms.RadioSelect(attrs={'class': 'checkbox checkbox-styled list-unstyled'}), required=True)
	grades_taught = forms.ModelMultipleChoiceField(queryset=Grade.objects.all(), widget=forms.CheckboxSelectMultiple(attrs={'class': 'checkbox checkbox-styled list-unstyled'}), required=True)
	subjects_taught = forms.ModelMultipleChoiceField(queryset=Subject.objects.all(), widget=forms.CheckboxSelectMultiple(attrs={'class': 'checkbox checkbox-styled list-unstyled'}), required=True)

	class Meta():
		model = User
		fields = ("email", "first_name", "last_name", "password1", "password2")

	def __init__(self, *args, **kwargs):
		super(TeacherRegisterForm, self).__init__(*args, **kwargs)
		self.fields['email'].widget.attrs={
            'class': 'form-control static dirty'}  
		self.fields['first_name'].widget.attrs={
            'class': 'form-control static dirty'} 
		self.fields['last_name'].widget.attrs={
            'class': 'form-control static dirty'} 
		self.fields['password1'].widget.attrs={
            'class': 'form-control static dirty'} 
		self.fields['password2'].widget.attrs={
            'class': 'form-control static dirty'} 
	
	def save(self):
		data = self.cleaned_data
		grades_taught = data['grades_taught']
		subjects_taught = data['subjects_taught']
		school = data['school']
		user = User(first_name=data['first_name'], last_name=data['last_name'], email=data['email'], is_teacher=True)
		user.set_password(data['password1'])
		user.save()
		teacher = Teacher.objects.create(user=user, school=school)
		teacher.save()
		for grade in grades_taught:
			teacher.grades_taught.add(grade)
		for subject in subjects_taught:
			teacher.subjects_taught.add(subject)
		return user

class StudentRegisterForm(UserCreationForm):
	class_code = forms.CharField()
	
	class Meta():
		model = User
		fields = ("email", "first_name", "last_name", "password1", "password2")
    
	def __init__(self, *args, **kwargs):
		super(StudentRegisterForm, self).__init__(*args, **kwargs)
		for visible in self.visible_fields():
			visible.field.widget.attrs['class'] = 'form-control'

	def save(self):
		data = self.cleaned_data
		class_code = data['class_code']
		classroom = Classroom.objects.get(class_code=class_code)
		print(classroom)
		if classroom:
			user = User(first_name=data['first_name'], last_name=data['last_name'], email=data['email'], is_student=True, user_must_change_password=False)
			user.set_password(data['password1'])
			user.save()
			student = Student.objects.create(user=user)
			classroom.students.add(student)
		else:
			raise forms.ValidationError('No class exists for that class code.  Please try again with a valid code.')
		return user


class JoinClassForm(forms.Form):
	class_code = forms.CharField()
	# class Meta():
	# 	model = User

class UserUpdateForm(ModelForm):
	class Meta():
		model = User
		fields = ("first_name", "last_name")

class ProfileUpdateForm(ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	rotate = forms.FloatField(widget=forms.HiddenInput())
	scaleX = forms.FloatField(widget=forms.HiddenInput())
	scaleY = forms.FloatField(widget=forms.HiddenInput())

	class Meta():
		model = Profile
		fields = ("photo", 'x', 'y', 'width', 'height', 'rotate', 'scaleX', 'scaleY',)

	def __init__(self, *args, **kwargs):
		super(ProfileUpdateForm, self).__init__(*args, **kwargs)
		self.fields['photo']=forms.FileField(widget=forms.FileInput(attrs={'id':'inputImage'}),label='Photo:', required=False)

	def save(self):
		photoImage = super(ProfileUpdateForm, self).save()

		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')
		r = self.cleaned_data.get('rotate')
		sx = self.cleaned_data.get('scaleX')
		sy = self.cleaned_data.get('scaleY')

		image = Image.open(photoImage.photo)
		format = image.format
		rotated_image = image.rotate(-r, expand=True)
		cropped_image = rotated_image.crop((x, y, w+x, h+y))
		cropped_image.thumbnail((200, 200), Image.ANTIALIAS)
		sfile = BytesIO()
		cropped_image.save(sfile, format=format)
		with storage.open(photoImage.photo.name, 'wb') as f:
		    f.write(sfile.getvalue())

		return photoImage

class UserLoginForm(forms.Form):
	email_address = forms.EmailField(widget=forms.EmailInput())
	password = forms.CharField(widget=forms.PasswordInput())


class PasswordChangeCustomForm(forms.Form):
	old_password = forms.CharField(required=False, label='Old Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	new_password = forms.CharField(required=False, label='New Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	new_password1 = forms.CharField(required=False, label='New Password Confirmation', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
