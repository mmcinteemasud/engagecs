from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy


class FirstPasswordReset(object):
	def process_request(self, request):
		if not request.user.is_anonymous:
			if request.user.user_must_change_password:
				path = request.path_info.lstrip('/')
				# If path is not root url ('') and path is not exempt from authentication
				if path != 'accounts/first-password-change/':
					if "admin" not in path:
						return HttpResponseRedirect(reverse_lazy('first_password_change'))
