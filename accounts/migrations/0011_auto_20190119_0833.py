# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2019-01-19 08:33
from __future__ import unicode_literals

from django.db import migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_user_user_must_change_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='photo',
            field=imagekit.models.fields.ProcessedImageField(blank=True, default='users/default_user_icon/default_user_icon.png', upload_to='users/%Y/%m/%d'),
        ),
    ]
