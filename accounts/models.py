from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from units.models import Subject, School, Grade, Unit
from django.db.models.signals import post_save
from django.dispatch import receiver
from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import SmartResize, ResizeToFit
import os
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage as storage
from io import BytesIO
from PIL import Image

#from classes.models import Classroom
# Create your models here.

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)



class User(AbstractUser):
    username = None
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    user_must_change_password = models.BooleanField(default=True)
    email = models.EmailField(_('email address'), unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):  # __unicode__ for Python 2
        return '%s %s - %s' % (self.first_name,self.last_name, self.email)

class Teacher(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	grades_taught = models.ManyToManyField(Grade, related_name='teachers', blank=True)
	subjects_taught = models.ManyToManyField(Subject, related_name='teachers', blank=True)
	school = models.ForeignKey(School, related_name='teachers', null=True, blank=True)
	units = models.ManyToManyField(Unit, related_name='teachers', blank=True)
    
	class Meta:
		ordering = ('school',)

	def __str__(self):  # __unicode__ for Python 2
		return self.user.email

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    
    def __str__(self):  # __unicode__ for Python 2
        return self.user.email

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    photo = ProcessedImageField(upload_to='users/%Y/%m/%d',default='users/default_user_icon/default_user_icon.png',
                                blank=True, 
                                           #processors=[ResizeToFit(500)],
                                           format='JPEG'
                                           #options={'quality': 60}
                                           )
    # def save(self, *args, **kwargs):
    #     if self.photo:
    #         img = Image.open(self.photo)
    #         resize = img.resize((240, 240), Image.ANTIALIAS)
    #         new_image = BytesIO()
    #         resize.save(new_image, format=img.format, quality=75)
    #         temp_name = os.path.split(self.photo.name)[1]
    #         self.photo.save(temp_name, content=ContentFile(new_image.getvalue()), save=False)
    #     super(Profile, self).save(*args, **kwargs)

    def __str__(self):
        return 'Profile for user {}'.format(self.user.email)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()