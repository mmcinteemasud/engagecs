from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from . import views
from mainpage.views import Mainpage

urlpatterns = [
    url(r'^login/$', Mainpage, name='login'),
	url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
	 # change password urls
    url(r'^password-change/$', views.ChangePasswordView,
        name='password_change'),
    url(r'^first-password-change/$', views.FirstChangePasswordView,
        name='first_password_change'),
    url(r'^password-change/done/$', auth_views.password_change_done,
        name='password_change_done'),
    # restore password urls
    url(r'^password-reset/$', auth_views.password_reset,
        name='password_reset'),
    url(r'^password-reset/done/$', auth_views.password_reset_done,
        name='password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$', auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^password-reset/complete/$', auth_views.password_reset_complete,
        name='password_reset_complete'),
    url(r'^register-teacher/$', views.TeacherRegistrationView.as_view(),
		name='teacher_registration'),
    url(r'^register-student/$', views.StudentRegistrationView.as_view(),
        name='student_registration'),
    url(r'^update/$', views.UserUpdateFormView,
		name='update_user'),
	url(r'^(?P<pk>\d+)/delete/$', views.UserDeleteFormView.as_view(),
		name='delete_user'),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

