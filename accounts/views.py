from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from urllib import request
from django.contrib.auth import update_session_auth_hash
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.db import transaction
from .forms import TeacherRegisterForm, StudentRegisterForm, UserUpdateForm, ProfileUpdateForm, PasswordChangeCustomForm
from django.contrib.auth.views import PasswordChangeView
from units.models import Unit, Grade, Subject
from .models import Teacher
from django.forms.models import model_to_dict
from classes.models import Classroom
from django.contrib import messages
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.


class TeacherRegistrationView(LoginRequiredMixin, UserPassesTestMixin, FormView):
    template_name = 'teachers/registration.html'
    form_class = TeacherRegisterForm
    success_url = reverse_lazy('manage_unit_list')

    def test_func(self):
        return self.request.user.is_staff

    def form_valid(self, form):
        result = super(TeacherRegistrationView, self).form_valid(form)
        cd = form.cleaned_data
        user=form.save()
        teacher = Teacher(user=user)
        grades=Grade.objects.filter(teachers=teacher)
        subjects=Subject.objects.filter(teachers=teacher)
        units = Unit.objects.filter(target_grades__in=[grade for grade in grades]).filter(subject__in=[subject for subject in subjects]).distinct()
        for unit in units:
            teacher.units.add(unit)
        data = {'message': 'Teacher added successfully.'}
        if self.request.is_ajax():
            return JsonResponse(data)
        else:
            return result

    def form_invalid(self, form):
        result = super(TeacherRegistrationView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return result

class StudentRegistrationView(FormView):
    template_name = 'mainpage/mainpage.html'
    form_class = StudentRegisterForm
    success_url = reverse_lazy('student_dashboard')

    def form_valid(self, form):
        result = super(StudentRegistrationView, self).form_valid(form)
        cd = form.cleaned_data
        class_code = cd['class_code']
        try:
            classroom = Classroom.objects.get(class_code=class_code)
        except:
            data = {'error': 'No classroom exists for that class code.  Please try again with a valid code.'}
            return JsonResponse(data)
        user = form.save()
        login(self.request, user)
        return result
    def form_invalid(self, form):
        result = super(StudentRegistrationView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return result


@login_required
@transaction.atomic
def UserUpdateFormView(request):
    if request.method == 'POST':
        if "cancel" in request.POST:
            url = reverse_lazy('login_success')	
            return HttpResponseRedirect(url)
        else:
            user_form = UserUpdateForm(request.POST, instance=request.user)
            profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
            if user_form.is_valid() and profile_form.is_valid():
                user_form.save()
                profile = profile_form.save()
                profile.user = request.user
                profile.save()
                return redirect("login_success")
            elif user_form.is_valid() and not profile_form.is_valid():
                user_form.save()
                return redirect("login_success")
            else:
                messages.error(request, ('Please correct the error below.'))
    else:
        user_form = UserUpdateForm(instance=request.user)
        profile_form = ProfileUpdateForm(instance=request.user)
    return render(request, 'accounts/user_update.html', {'user_form':user_form, 'profile_form':profile_form})


class UserDeleteFormView(DeleteView):
	model = User
	fields = ['email']
	success_url = reverse_lazy('mainpage')

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			return redirect("login_success")
		else:
			return super(UserDeleteFormView, self).post(request, *args, **kwargs)


@login_required
def ChangePasswordView(request):
    MIN_LENGTH = 8    
    if request.method == 'POST':
        form = PasswordChangeCustomForm(request.POST)
        user = request.user
        if "cancel" in request.POST:
            url =reverse_lazy('login_success')
            return HttpResponseRedirect(url)
        else:
            if form.is_valid():
                cd = form.cleaned_data
                old_pwd = cd['old_password']
                new_pwd1 = cd['new_password']
                new_pwd2 = cd['new_password1']
                print(new_pwd1, new_pwd2, "first")
                if authenticate(email=request.user.email, password=old_pwd):
                    if len(new_pwd1) < MIN_LENGTH:
                        messages.error(request,"The new password must be at least %d characters long." % MIN_LENGTH)

                    # At least one letter and one non-letter
                    first_isalpha = new_pwd1[0].isalpha()
                    if all(c.isalpha() == first_isalpha for c in new_pwd1):
                        messages.error(request,"The new password must contain at least one letter and at least one digit or" \
                                        " punctuation character.")
                        return render(request,'registration/password_change_form.html' , {'form': form})
                    if new_pwd1 != new_pwd2:
                        messages.error(request, 'Your new passwords do not match.')
                    else:
                        user.set_password(new_pwd1)
                        user.save()
                        update_session_auth_hash(request, user)
                        url = reverse_lazy('login_success')
                        return HttpResponseRedirect(url) 
                else:
                        messages.error(request, 'Please enter your old password correctly.')
                
    else:
        form = PasswordChangeCustomForm()
    return render(request,'registration/password_change_form.html' , {'form': form})

@login_required
def FirstChangePasswordView(request):
    MIN_LENGTH = 8    
    if request.method == 'POST':
        form = PasswordChangeCustomForm(request.POST)
        user = request.user
        if "cancel" in request.POST:
            url = reverse_lazy('login_success')
            return HttpResponseRedirect(url)
        else:
            if form.is_valid():
                cd = form.cleaned_data
                old_pwd = cd['old_password']
                new_pwd1 = cd['new_password']
                new_pwd2 = cd['new_password1']
                if authenticate(email=request.user.email, password=old_pwd):
                    if len(new_pwd1) < MIN_LENGTH:
                        messages.error(request,"The new password must be at least %d characters long." % MIN_LENGTH)

                    # At least one letter and one non-letter
                    first_isalpha = new_pwd1[0].isalpha()
                    if all(c.isalpha() == first_isalpha for c in new_pwd1):
                        messages.error(request,"The new password must contain at least one letter and at least one digit or" \
                                        " punctuation character.")
                        return render(request,'registration/password_change_form.html' , {'form': form})
                    if new_pwd1 == new_pwd2:
                        user.set_password(new_pwd1)
                        user.user_must_change_password = False
                        user.save()
                        update_session_auth_hash(request, user)
                        url = reverse_lazy('login_success')
                        return HttpResponseRedirect(url) 
                    else:
                        messages.error(request, 'Your new passwords do not match.')
                else:
                        messages.error(request, 'Please enter your old password correctly.')
                
    else:
        form = PasswordChangeCustomForm()
    return render(request,'registration/first_password_change_form.html' , {'form': form})
