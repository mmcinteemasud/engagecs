from django.contrib import admin
from .models import Classroom, Assignment, AssignmentFile, AssignmentSubmission, SubmissionFile
from units.models import Unit
from accounts.models import Student


# Register your models here.
class UnitInline(admin.StackedInline):
	model = Unit

class StudentInline(admin.StackedInline):
	model = Student

class FileInLine(admin.StackedInline):
	model = AssignmentFile

@admin.register(Classroom)
class ClassAdmin(admin.ModelAdmin):
	list_display = ['title']
	inline = [UnitInline, StudentInline]

@admin.register(Assignment)
class AssignmentAdmin(admin.ModelAdmin):
	list_display = ['title']
	inline = [FileInLine,]

@admin.register(AssignmentFile)
class AssignmentFileAdmin(admin.ModelAdmin):
	list_display = ['file']

class SubmissionFileInline(admin.StackedInline):
	model = SubmissionFile

@admin.register(AssignmentSubmission)
class AssignmentSubmissionAdmin(admin.ModelAdmin):
	list_display = ['assignment']
	inline = [SubmissionFileInline,]

