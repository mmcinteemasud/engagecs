from __future__ import unicode_literals

from django import VERSION

from accounts.models import Teacher, Student
from django.contrib.auth import get_user_model
User = get_user_model()

def user_objects(request):
	"""Provide the count of unread messages for an authenticated user."""
	if not request.user.is_anonymous:
		if request.user.is_teacher:
			teacher = Teacher.objects.get(user=request.user)
			return {'teacher': teacher}
		elif request.user.is_student:
			student = Student.objects.get(user=request.user)
			return {'student': student}
		else:
			return {}
	else:
		return {}

