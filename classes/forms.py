from django import forms
from django.forms import ModelForm
from classes.models import Classroom, Assignment, AssignmentFile, AssignmentCategory, AssignmentSubmission, SubmissionFile
from ckeditor_uploader.widgets import *
from ckeditor.widgets import CKEditorWidget
from django.forms import BaseModelFormSet
from django.forms import modelformset_factory
from datetimewidget.widgets import DateTimeWidget


class ClassUpdateForm(ModelForm):

	class Meta:
		model = Classroom
		fields = ['title','units','students']

	def __init__(self, units, students, *args, **kwargs):
		super(ClassUpdateForm, self).__init__(*args, **kwargs)
		self.fields['units'].queryset = units
		self.fields['students'].queryset = students
		self.fields['title'].widget.attrs={
            'class': 'form-control static dirty'}

dateTimeOptions = {
	'format': 'yyyy-m-dd hh:ii',
	'autoclose': True,
	'showMeridian' : True
}

class AssignmentCreateForm(ModelForm):
	#assign_to_other_classes = forms.ModelMultipleChoiceField(queryset=Classroom.objects.all(), widget=forms.CheckboxSelectMultiple, required=True)

	class Meta():
		model = Assignment
		fields = ["title", "description", "maximum_points", "allow_upload", "due_date"]

		widgets = {
            #Use localization and bootstrap 3
            'body': CKEditorWidget(config_name='user_ckeditor'),
            'due_date': DateTimeWidget(attrs={'id':"due_date"}, options=dateTimeOptions, bootstrap_version=3)
        }

	def __init__(self, assignment_categories, *args, **kwargs):
		super(AssignmentCreateForm, self).__init__(*args, **kwargs)
		self.fields['category'] = forms.ModelChoiceField(
        	queryset=assignment_categories,
            required=True
        )
		self.initial['category'] = assignment_categories[0]
		self.fields['due_date'].required = True
		self.fields['maximum_points'].required = True
		self.fields['title'].widget.attrs={
            'class': 'form-control static dirty'}
		self.fields['maximum_points'].widget.attrs={
            'class': 'form-control static dirty'}
		self.fields['category'].widget.attrs={
            'class': 'btn btn-default dropdown-toggle'}

class FileUploadForm(ModelForm):
	
	class Meta():
		model = AssignmentFile
		fields = ['file']

	def __init__(self, *args, **kwargs):
		super(FileUploadForm, self).__init__(*args, **kwargs)
		self.fields['file'].required = False
		self.fields['file2'] = forms.FileField(label="Files", widget=forms.FileInput(attrs={'id':'file2'}), required=False)
		self.fields['file3'] = forms.FileField(label="Files", widget=forms.FileInput(attrs={'id':'file3'}), required=False)
		self.fields['file4'] = forms.FileField(label="Files", widget=forms.FileInput(attrs={'id':'file4'}), required=False)

FileUploadFormSet = modelformset_factory(AssignmentFile, fields=('file',), extra=1, max_num=4)

class BaseCategoryUpdateFormSet(BaseModelFormSet):
	def __init__(self, classroom_id, *args, **kwargs):
		super(BaseCategoryUpdateFormSet, self).__init__(*args, **kwargs)
		classroom = Classroom.objects.get(id=classroom_id)
		self.queryset = AssignmentCategory.objects.filter(classroom=classroom)
		for form in self.forms:
			for field in form.fields:
				form.fields[field].widget.attrs.update({'class': 'form-control static dirty'})

CategoryUpdateFormSet = modelformset_factory(AssignmentCategory, fields=('title', 'weight'), extra=0, formset=BaseCategoryUpdateFormSet)

class AssignmentSubmissionForm(ModelForm):
	file = forms.FileField()
	
	class Meta():
		model = AssignmentSubmission
		fields = ["file"]
