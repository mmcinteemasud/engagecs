# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-04-13 18:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_remove_teacher_classrooms'),
        ('classes', '0002_classroom_units'),
    ]

    operations = [
        migrations.AddField(
            model_name='classroom',
            name='teacher',
            field=models.ForeignKey(blank=True, default=5, on_delete=django.db.models.deletion.CASCADE, related_name='classrooms', to='accounts.Teacher'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='classroom',
            name='students',
            field=models.ManyToManyField(blank=True, related_name='classrooms', to='accounts.Student'),
        ),
        migrations.AlterField(
            model_name='classroom',
            name='units',
            field=models.ManyToManyField(blank=True, related_name='classrooms', to='units.Unit'),
        ),
    ]
