# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-05-04 20:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classes', '0006_assignment_assignmentcategories_assignmentfiles'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assignment',
            name='classroom',
        ),
        migrations.AddField(
            model_name='assignment',
            name='classroom',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, related_name='assignments', to='classes.Classroom'),
            preserve_default=False,
        ),
    ]
