from django.db import models
from units.models import Unit, Module
from accounts.models import Student, Teacher
from django.utils.crypto import get_random_string
from django.utils.text import slugify
import os
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField

# Create your models here.
class Classroom(models.Model):
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200, unique=True)
	class_code = models.CharField(max_length=10, unique=True)
	teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name='classrooms', blank=True)
	students = models.ManyToManyField(Student, related_name='classrooms', blank=True)
	units = models.ManyToManyField(Unit, related_name='classrooms', blank=True)
	class Meta:
		ordering = ('title',)

	def __str__(self):  # __unicode__ for Python 2
		return self.title

	def _get_unique_slug(self):
		slug = slugify(self.title)
		unique_slug = slug
		num = 1
		while Classroom.objects.filter(slug=unique_slug).exists():
			unique_slug = '{}-{}'.format(slug, num)
			num += 1
		return unique_slug

	def _get_unique_code(self):
		code = get_random_string(length=7, allowed_chars=u'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
		unique_code = code
		num = 1
		while Classroom.objects.filter(class_code=unique_code).exists():
			unique_code = '{}-{}'.format(code, num)
			num += 1
		return unique_code
 
	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = self._get_unique_slug()
		if not self.class_code:
			self.class_code = self._get_unique_code()
		super().save()

class AssignmentCategory(models.Model):
	title = models.CharField(max_length=200)
	classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE, related_name='assignment_categories')
	weight = models.IntegerField()
	class Meta:
		ordering = ('weight',)

	def __str__(self):  # __unicode__ for Python 2
		return self.title

class Assignment(models.Model):
	title = models.CharField(max_length=200)
	description = RichTextField(config_name='user_ckeditor', blank=True)
	maximum_points = models.IntegerField(blank=True)
	allow_upload = models.BooleanField()
	classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE, related_name='assignments', blank=True)
	assign_date = models.DateTimeField(auto_now_add=True,
                            db_index=True, blank=True)
	due_date = models.DateTimeField(auto_now_add=False,
                            db_index=True, blank=True)
	category = models.CharField(max_length=200, blank=True)
	
	class Meta:
		ordering = ('-assign_date',)

	def __str__(self):  # __unicode__ for Python 2
		return self.title

	
class AssignmentFile(models.Model):
	file = models.FileField(upload_to="class_assignments/%Y/%m/%d")
	assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, related_name='files')

	def filename(self):
		return os.path.basename(self.file.name)

class AssignmentSubmission(models.Model):
	student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='submissions')
	assignment = assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, related_name='submissions')
	score = models.IntegerField(blank=True, null=True)
	teacher_comments = models.TextField(blank=True)

	def __str__(self):  # __unicode__ for Python 2
		return self.assignment.title

class SubmissionFile(models.Model):
	file = models.FileField(upload_to="assignments_submissions/%Y/%m/%d", null=True)	
	pdf_file = models.FileField(upload_to="assignments_submissions/pdf/%Y/%m/%d", null=True)
	assignment_submission = models.ForeignKey(AssignmentSubmission, on_delete=models.CASCADE, related_name='files')
	submission_date = models.DateTimeField(auto_now_add=True,
                            db_index=True, blank=True)
	def filename(self):
		return os.path.basename(self.file.name)