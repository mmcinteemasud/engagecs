from __future__ import absolute_import

from celery import shared_task
from .models import SubmissionFile
from django.core.files import File
import datetime
import os
from django.conf import settings
from pylokit import Office


@shared_task
def test(param):
    return 'The test task executed with argument "%s" ' % param

@shared_task
def convertFileToPDF(path, filename, submission_file_id):
	#tmp_file = os.path.join(settings.MEDIA_ROOT, path)
	lo_path = "/usr/lib/libreoffice/program"
	with Office(lo_path) as lo:
		with lo.documentLoad(path) as doc:
			pdf_path = 'assignments_submissions/pdf' + datetime.datetime.now().strftime("/%Y/%m/%d/") + filename + '.pdf'
			file_path = os.path.join(settings.MEDIA_ROOT, pdf_path)
			relative_path = 'media/' + pdf_path
			pdf_tmp = doc.saveAs(relative_path)
	submission_file = SubmissionFile.objects.filter(id=submission_file_id).update(pdf_file=pdf_path)

	#os.remove(pdf_path)