from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def make_label_id(var):
	var_clean = var.replace(" ", "_")
	id = "id_" + var_clean
	return id

@register.filter
def replace_space_with_underscore(var):
	return var.replace(" ", "_")

@register.filter
def date_label_id(var):
	var_clean = var.replace(" ", "_")
	id = "id_" + var_clean + "_Due"
	return id