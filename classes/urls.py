from django.conf.urls import url
from . import views


urlpatterns = [

	url(r'^create-class/$', views.ClassCreateFormView.as_view(),
		name='create_class'),
	url(r'^class-detail/(?P<pk>\d+)/$', views.ClassroomDetailView.as_view(),
		name='class_detail'),
	url(r'^join-class/$', views.JoinClassFormView.as_view(),
		name='join_class'),
	url(r'^(?P<pk>\d+)/update/$', views.ClassUpdateFormView.as_view(),
		name='update_class'),
	url(r'^(?P<pk>\d+)/delete/$', views.ClassDeleteFormView.as_view(),
		name='delete_class'),
	url(r'^create-assignment/(?P<classroom_id>\d+)/$', views.AssignmentCreateFormView.as_view(),
		name='create_assignment'),
	url(r'^update-assignment-categories/(?P<classroom_id>\d+)/$', views.AssignmentCategoryUpdateFormView.as_view(),
		name='update_assignment_categories'),
	url(r'^assignments/(?P<classroom_id>\d+)/$', views.AssignmentListView.as_view(),
		name='assignment_list'),
	url(r'^assignment/(?P<pk>\d+)/$', views.AssignmentDetailView.as_view(),
		name='assignment_detail'),
	url(r'^(?P<pk>\d+)/update_assignment/$', views.AssignmentUpdateFormView.as_view(),
		name='update_assignment'),
	url(r'^(?P<pk>\d+)/delete_assignment/$', views.AssignmentDeleteFormView.as_view(),
		name='delete_assignment'),
	url(r'^submit-assignment/(?P<assignment_id>\d+)/(?P<classroom_id>\d+)/$', views.AssignmentSubmitFormView.as_view(),
		name='submit_assignment'),
	url(r'^submissions/(?P<pk>\d+)/$', views.StudentSubmissionsView.as_view(),
		name='student_submissions'),
	url(r'^file-annotate-view/(?P<classroom_id>\d+)/(?P<pk>\d+)/$', views.FileAnnotateView.as_view(),
		name='file_annotate'),
	url(r'^students/(?P<classroom_id>\d+)/$', views.StudentListView.as_view(),
		name='student_list'),
	]