from django.shortcuts import render
from urllib import request
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from teachers.forms import ClassCreateForm
from accounts.forms import JoinClassForm
from .forms import AssignmentCreateForm, FileUploadForm, AssignmentSubmissionForm, CategoryUpdateFormSet, FileUploadFormSet, ClassUpdateForm
from django.contrib.auth import authenticate, login
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin
from accounts.models import Teacher, Student
from units.models import Content
from .models import Classroom, Assignment, AssignmentCategory, AssignmentFile, AssignmentSubmission, SubmissionFile
from django.contrib import messages
from datetime import datetime
import os
from django.core.files.storage import default_storage
from .tasks import convertFileToPDF
from django.contrib.auth import get_user_model
User = get_user_model()


# Create your views here.
class ClassCreateFormView(LoginRequiredMixin, FormView):
	form_class = ClassCreateForm
	template_name = 'teachers/unit/classes.html'
	success_url = reverse_lazy('teacher_classroom_list')

	def get_form_kwargs(self):
		kwargs = super(ClassCreateFormView, self).get_form_kwargs()
		teacher = Teacher.objects.get(user=self.request.user)
		units = teacher.units.all()
		kwargs['units'] = units
		return kwargs

	def form_valid(self, form):
		result = super(ClassCreateFormView, self).form_valid(form)
		teacher = Teacher.objects.get(user=self.request.user)
		title = form.cleaned_data['title']
		units = form.cleaned_data['add_units']
		classroom = Classroom.objects.create(title=title, teacher=teacher)
		for unit in units:
			classroom.units.add(unit)
		AssignmentCategory.objects.create(title="Homework", weight=20, classroom=classroom)
		AssignmentCategory.objects.create(title="Quizzes", weight=30, classroom=classroom)
		AssignmentCategory.objects.create(title="Tests", weight=50, classroom=classroom)
		return result
	def form_invalid(self, form):
		result = super(ClassCreateFormView, self).form_invalid(form)
		return result

class ClassroomDetailView(LoginRequiredMixin, DetailView):
	model = Classroom
	template_name = 'classes/class_detail.html'
	def get_context_data(self, **kwargs):
		context = super(ClassroomDetailView, self).get_context_data(**kwargs)
		context['classroom'] = self.get_object()
		return context


class JoinClassFormView(FormView):
	template_name = 'students/unit/student_dashboard.html'
	form_class = JoinClassForm
	success_url = reverse_lazy('student_dashboard')

	def form_valid(self, form):
		result = super(JoinClassFormView, self).form_valid(form)
		data = form.cleaned_data
		class_code = data['class_code']
		
		try:
			classroom = Classroom.objects.get(class_code=class_code)
			student = Student.objects.get(user=user)
			classroom.students.add(student)
			return result
				
		except:
			data = {'error': 'No classroom exists for that class code.  Please try again with a valid code.'}
			return JsonResponse(data)

	def form_invalid(self, form):
		result = super(JoinClassFormView, self).form_valid(form)
		if self.request.is_ajax():
			return JsonResponse(form.errors, status=400)
		else:
			return result

class ClassUpdateFormView(LoginRequiredMixin, UpdateView):
	model = Classroom
	form_class = ClassUpdateForm
	template_name_suffix = '_update_form'
	success_url = reverse_lazy('teacher_classroom_list')

	def get_form_kwargs(self):
		kwargs = super(ClassUpdateFormView, self).get_form_kwargs()
		teacher = Teacher.objects.get(user=self.request.user)
		classroom = self.get_object()
		students = classroom.students.all()
		units = teacher.units.all()
		kwargs['units'] = units
		kwargs['students'] = students
		return kwargs


	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			url = reverse_lazy('class_detail', args=[self.get_object().id])
			return HttpResponseRedirect(url)				
		else:
			return super(ClassUpdateFormView, self).post(request, *args, **kwargs)

class ClassDeleteFormView(LoginRequiredMixin, DeleteView):
	model = Classroom
	fields = ['title']
	success_url = reverse_lazy('teacher_classroom_list')
 
	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			url = reverse_lazy('class_detail', args=[self.get_object().id])
			return HttpResponseRedirect(url)				
		else:
			return super(ClassDeleteFormView, self).post(request, *args, **kwargs)

class AssignmentCreateFormView(LoginRequiredMixin, FormView):
	form_class = AssignmentCreateForm
	template_name = 'classes/assignment_create_form.html'
	success_url = reverse_lazy('teacher_classroom_list')

	def get_context_data(self, **kwargs):
		context = super(AssignmentCreateFormView, self).get_context_data(**kwargs)
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		units = classroom.units.all()
		teacher=Teacher.objects.get(user=self.request.user)
		other_classes = Classroom.objects.filter(teacher=teacher, units=units).exclude(pk=classroom.id)
		# clear formset from any previously uploaded files
		file_upload_form = FileUploadFormSet(queryset=AssignmentFile.objects.none())
		context['file_upload_form'] = file_upload_form
		context['other_classes'] = other_classes
		context['classroom'] = classroom
		return context

	def get_form_kwargs(self):
		kwargs = super(AssignmentCreateFormView, self).get_form_kwargs()
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		assignment_categories = AssignmentCategory.objects.filter(classroom=classroom)
		kwargs['assignment_categories'] = assignment_categories
		return kwargs

	def get_initial(self, **kwargs):		
		initial = super(AssignmentCreateFormView, self).get_initial()
		if self.request.method == 'GET':
			if 'content' in self.request.GET:
			 	content_id = self.request.GET['content']
			 	content = Content.objects.get(id=content_id)
			 	title = content.item.title
			 	questions = content.item.questions
			else:
				return initial
        	# update initial field defaults with custom set default values:
			initial.update({'title': title, 'description': questions})
		return initial

	def form_valid(self, form):
		result = super(AssignmentCreateFormView, self).form_valid(form)
		assign_date = datetime.now()
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		data = form.cleaned_data
		title = data['title']
		description = data['description']
		maximum_points = data['maximum_points']
		allow_upload = data['allow_upload']
		due_date= data['due_date']
		category = data['category']
		assignment = Assignment(title=title, description=description, maximum_points=maximum_points, allow_upload=allow_upload, 
			classroom=classroom, assign_date=assign_date, due_date=due_date, category=category)
		assignment.save()
		form_set = FileUploadFormSet(self.request.POST, self.request.FILES)
		for form in form_set: 
			try:
				data = form.cleaned_data
				file = data['file']
				file_now = form.save(commit=False)
				file_now.file = file
				file_now.assignment = assignment
				file_now.save()
			except:
				pass
		other_classes1=self.request.POST.getlist('assign_to_other_classes')
		for each in other_classes1:
			classroom_now = Classroom(id=each)
			due_date = self.request.POST.get(each)
			assignment_now = Assignment(title=title, description=description, maximum_points=maximum_points, allow_upload=allow_upload, 
				classroom=classroom_now, assign_date=assign_date, due_date=due_date, category=category)
			assignment_now.save()
			try: 
				files_now = AssignmentFile.objects.filter(assignment=assignment)
			except:
				files_now=None
			if files_now:
				for each in files_now:
					each.pk = None
					each.assignment = assignment_now
					each.save()

		url = reverse_lazy('class_detail', args=[classroom.id])
		return HttpResponseRedirect(url)


class AssignmentListView(LoginRequiredMixin, TemplateView):
	model = Assignment
	template_name = 'classes/assignment_list.html'

	def get_context_data(self, **kwargs):
		context = super(AssignmentListView, self).get_context_data(**kwargs)
		# get assignment object
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		context['classroom'] = classroom
		return context

class AssignmentCategoryUpdateFormView(LoginRequiredMixin, FormView):
	form_class = CategoryUpdateFormSet
	template_name = 'classes/assignment_category_update_form.html'
	success_url = reverse_lazy('teacher_classroom_list')

	def get_form_kwargs(self):
		kwargs = super(AssignmentCategoryUpdateFormView, self).get_form_kwargs()
		kwargs["classroom_id"] = self.kwargs['classroom_id']
		return kwargs

	def get_context_data(self, **kwargs):
		context = super(AssignmentCategoryUpdateFormView, self).get_context_data(**kwargs)
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		context['classroom'] = classroom
		return context

	def form_valid(self, form):
		total_weight = 0
		category_titles = []
		for category in form:
			data = category.cleaned_data
			weight = data.get('weight')
			title = data.get('title')
			print(weight, title, 'here')
			if title != None:
				category_titles.append(title)
			if weight != None:
				total_weight += data.get('weight')
		if total_weight == 100:	
			classroom = Classroom(id=self.kwargs['classroom_id'])
			categories = AssignmentCategory.objects.filter(classroom=classroom)
			for category in form:
				if category.has_changed():
					instance = category.save(commit=False)
					instance.classroom = classroom
					instance.save()
			for each in categories:
				if each.title not in category_titles:
					each.delete()
			category_titles=[]
		else:
			classroom = Classroom(id=self.kwargs['classroom_id'])
			error = 'Total weight must equal 100.  Try again.'
			return render(self.request, 'classes/assignment_category_update_form.html', {'form': form, 'error': error, 'classroom':classroom})
		url = reverse_lazy('class_detail', args=[self.kwargs['classroom_id']])
		return HttpResponseRedirect(url)

	def form_invalid(self, form):
		result = super(AssignmentCategoryUpdateFormView, self).form_invalid(form)
		print('form invalid', form.errors)
		return result

class AssignmentDetailView(LoginRequiredMixin, DetailView):
	model = Assignment
	template_name = 'classes/assignment_detail.html'

	def get_context_data(self, **kwargs):
		context = super(AssignmentDetailView, self).get_context_data(**kwargs)
		# get assignment object
		assignment = self.get_object()
		context['assignment'] = assignment
		if self.request.user.is_student:
			student = Student.objects.get(user=self.request.user)
			form = AssignmentSubmissionForm()
			context['form'] = form
			context['classroom_id'] = assignment.classroom.id
			context['classroom'] = assignment.classroom
			try:
				submission = AssignmentSubmission.objects.get(student=student, assignment=assignment)
				context['submission'] = submission
			except:
				pass
		if self.request.user.is_teacher:
			context['classroom'] = assignment.classroom
		return context

class AssignmentUpdateFormView(LoginRequiredMixin, UpdateView):
	model = Assignment
	fields = ['title','description']
	template_name_suffix = '_update_form'
	def get_context_data(self, **kwargs):
		context = super(AssignmentUpdateFormView, self).get_context_data(**kwargs)
		context['classroom'] = self.get_object().classroom
		return context
	def get_success_url(self, *args, **kwargs):         
		return reverse_lazy('assignment_detail', args=[self.get_object().id])

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			url = reverse_lazy('class_detail', args=[self.get_object().classroom.id])
			return HttpResponseRedirect(url)				
		else:
			return super(AssignmentUpdateFormView, self).post(request, *args, **kwargs)

class AssignmentDeleteFormView(LoginRequiredMixin, DeleteView):
	model = Assignment
	fields = ['title']

	def get_context_data(self, **kwargs):
		context = super(AssignmentDeleteFormView, self).get_context_data(**kwargs)
		context['classroom'] = self.get_object().classroom
		return context	

	def get_success_url(self):         
		return reverse_lazy('class_detail', args=[self.get_object().classroom.id])

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			url = reverse_lazy('class_detail', args=[self.get_object().classroom.id])
			return HttpResponseRedirect(url)				
		else:
			return super(AssignmentDeleteFormView, self).post(request, *args, **kwargs)


class AssignmentSubmitFormView(LoginRequiredMixin, FormView):
	form_class = AssignmentSubmissionForm
	template_name = 'classes/assignment_detail.html'
	

	def get_success_url(self, request, *args, **kwargs):
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		return reverse_lazy('class_detail', args=[classroom.id])

	def form_valid(self, form):
		data = form.cleaned_data
		file = self.request.FILES['file']
		filename, file_extension = os.path.splitext(file.name)
		assignment = Assignment(id=self.kwargs['assignment_id'])
		student = Student(user=self.request.user)
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		try:
			submission = AssignmentSubmission.objects.get(student=student, assignment=assignment)
		except:
			submission = AssignmentSubmission.objects.create(student=student, assignment=assignment)
		if file_extension != '.pdf':
			path = 'tmp/' + file.name
			# with default_storage.open(path, 'wb+') as destination:
			# 	for chunk in file.chunks():
			# 		destination.write(chunk)

			submission_file = SubmissionFile.objects.create(file=file, assignment_submission=submission)
			filename, file_extension = os.path.splitext(submission_file.filename())
			convertFileToPDF.delay(submission_file.file.path, filename, submission_file.id)
		else:
			pdf_file = file
			submission_file = SubmissionFile.objects.create(pdf_file=pdf_file, assignment_submission=submission)
			pdf_file_path = submission_file.pdf_file
			submission_file.file=pdf_file_path
			submission_file.save()
		url = reverse_lazy('class_detail', args=[classroom.id])
		return HttpResponseRedirect(url)

	def post(self, request, *args, **kwargs):
		if "cancel" in request.POST:
			classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
			url = reverse_lazy('class_detail', args=[classroom.id])
			return HttpResponseRedirect(url)
				
		else:
			return super(AssignmentSubmitFormView, self).post(request, *args, **kwargs)

class StudentListView(LoginRequiredMixin, TemplateView):
	model = Classroom
	template_name = 'classes/student_list.html'

	def get_context_data(self, **kwargs):
		context = super(StudentListView, self).get_context_data(**kwargs)
		# get student object
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		context['classroom'] = classroom
		return context

class StudentSubmissionsView(LoginRequiredMixin, DetailView):
	model = Assignment
	template_name = 'classes/student_submissions.html'

	def get_context_data(self, **kwargs):
		context = super(StudentSubmissionsView, self).get_context_data(**kwargs)
		context['assignment'] = self.get_object()
		context['classroom'] = self.get_object().classroom
		return context


class FileAnnotateView(LoginRequiredMixin, DetailView):
	model = SubmissionFile
	template_name = 'classes/file_annotate.html'

	def get_context_data(self, **kwargs):
		context = super(FileAnnotateView, self).get_context_data(**kwargs)
		classroom = Classroom.objects.get(id=self.kwargs['classroom_id'])
		context['classroom'] = classroom
		context['file'] = self.get_object()	
		return context