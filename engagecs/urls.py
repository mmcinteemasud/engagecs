"""labworx URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from units.views import UnitListView
from ajax_select import urls as ajax_select_urls

urlpatterns = [
    url(r'^messages/lookups/', include(ajax_select_urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^unit/', include('units.urls')),
    url(r'^units/$', UnitListView.as_view(), name='unit_list'),
    url(r'^teachers/', include('teachers.urls')),
    url(r'^classes/', include('classes.urls')),
    url(r'^students/', include('students.urls')),
    url(r'^', include('mainpage.urls')),
    url(r'^messages/', include('postman.urls', namespace='postman', app_name='postman')),

  

   

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
