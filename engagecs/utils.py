from django.core.mail import EmailMultiAlternatives


def send_email(subject, message, from_email, recipient_list, **kwargs):
    # Create message

    html_message = kwargs.pop('html_message', None)
    send_kwargs = {}
    fail_silently = kwargs.pop('fail_silently', None)
    if fail_silently is not None:
        send_kwargs['fail_silently'] = fail_silently
    msg = EmailMultiAlternatives(subject, message, from_email, recipient_list, **kwargs)
    if html_message:
        msg.attach_alternative(html_message, 'text/html')
    return msg.send(**send_kwargs)