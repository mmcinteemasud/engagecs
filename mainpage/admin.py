from django.contrib import admin
from .models import RequestClient, RequestGradeLevel

# Register your models here.

@admin.register(RequestGradeLevel)
class RequestGradeLevelAdmin(admin.ModelAdmin):
	list_display = ['description']
	

@admin.register(RequestClient)
class RequestClientAdmin(admin.ModelAdmin):
	list_display = ['fullname', 'email', 'date_requested']

