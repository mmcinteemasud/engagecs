from django import forms
from django.forms import ModelForm
from .models import RequestClient, RequestGradeLevel


GRADE_LEVEL_CHOICES = (
        ('1', 'Elementary / Key Stages 1-2 / PYP'),
        ('2', 'Middle School / Key Stage 3 / MYP'),
        ('3', 'High School / Key Stages 4-5 / DP'),
    )

class RequestInfoForm(ModelForm):
	
	class Meta():
		model = RequestClient
		fields = ["fullname", "email"]

	def __init__(self, *args, **kwargs):
		super(RequestInfoForm, self).__init__(*args, **kwargs)
		self.fields['grade_level_choices'] = forms.MultipleChoiceField(
        	choices=GRADE_LEVEL_CHOICES,
        	widget=forms.CheckboxSelectMultiple,
            required=True,
            label='',
        )
		# self.fields["title"] = forms.CharField(
		# 	label="Class Title",
  #           widget=forms.TextInput(attrs={'class':'form-control'})
  #           )