from django.db import models

# Create your models here.


class RequestClient(models.Model):
	fullname = models.CharField(max_length=200)
	email = models.EmailField(max_length=200)
	date_requested =  models.DateTimeField(auto_now_add=True,
                            db_index=True)
	class Meta:
		ordering = ('-date_requested',)

	def __str__(self):  # __unicode__ for Python 2
		return '%s - %s - %s' % (self.fullname,self.email, self.date_requested)

class RequestGradeLevel(models.Model):

	request_client = models.ManyToManyField(RequestClient, related_name='grade_levels')
	description = models.CharField(max_length=350)

	def __str__(self):  # __unicode__ for Python 2
		return self.description