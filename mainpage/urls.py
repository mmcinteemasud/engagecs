from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^$', views.Mainpage, name='mainpage'),
	url(r'^login-success/$', views.Login_Success, name='login_success'),
	url(r'^staff-dashboard/$', views.StaffDashboardView.as_view(), name='staff_dashboard'),
	url(r'^learn-more/$', views.RequestInfoFormView.as_view(), name='request_info'),

]