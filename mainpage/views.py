from django.shortcuts import render
from django.shortcuts import redirect
from urllib import request
from django.http import HttpResponseRedirect
from accounts.forms import StudentRegisterForm, JoinClassForm, UserLoginForm
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth import authenticate, login
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from .forms import RequestInfoForm, GRADE_LEVEL_CHOICES
from .models import RequestClient, RequestGradeLevel
from django.http import JsonResponse
from django.contrib.auth import get_user_model
User = get_user_model()
from postman.api import pm_write
from postman.tasks import sendEmail
from django.conf import settings
from django.template.loader import render_to_string
from datetime import datetime
from django.views.generic import TemplateView

# Create your views here.

def Mainpage(request):
    if request.user.is_authenticated:
        return redirect("login_success")
    register_form = StudentRegisterForm()
    join_class_form = JoinClassForm()
    login_form = UserLoginForm()
    password_reset_form = PasswordResetForm()
    if request.method == 'POST':
        login_form = UserLoginForm(request.POST)
        if login_form.is_valid():
            data = login_form.cleaned_data
            email = data['email_address']
            password = data['password']
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                
            else:
                login_error = "Please use the correct username and password"
                return render(request, 'mainpage/mainpage.html', {'register_form':register_form, 'join_class_form':join_class_form, 'login_form':login_form, 'login_error':login_error, 'form':password_reset_form})
            return redirect("login_success")

        else:
            return render(request, 'mainpage/mainpage.html', {'register_form':register_form, 'join_class_form':join_class_form, 'login_form':login_form, 'form':password_reset_form})
   
    return render(request, 'mainpage/mainpage.html', {'register_form':register_form, 'join_class_form':join_class_form, 'login_form':login_form, 'form':password_reset_form})

def Login_Success(request):
    """
    Redirects users based on type or group
    """
    if request.user.groups.filter(name="Instructors").exists() or request.user.is_superuser:
        # user is an admin
        return redirect("staff_dashboard")
    if request.user.is_teacher:
        return redirect("teacher_dashboard")
    else:
        return redirect("student_dashboard")

class StaffDashboardView(TemplateView):
    template_name = "mainpage/staff_dashboard.html"


class RequestInfoFormView(FormView):
    form_class = RequestInfoForm
    template_name = 'mainpage/info_request.html'
    success_url = reverse_lazy('mainpage')
    sender = User.objects.get(email='mcinteemasud@gmail.com')
    recipient = User.objects.get(email='viceliasrodriguez@gmail.com')

    def form_valid(self, form):
        result = super(RequestInfoFormView, self).form_valid(form)
        form_data = form.cleaned_data
        fullname = form_data['fullname']
        email = form_data['email']
        choices = dict(GRADE_LEVEL_CHOICES)
        client = RequestClient.objects.create(fullname=fullname, email=email)
        grade_level_choices_list = []
        for choice in form_data['grade_level_choices']:
            try:
                choice_now = choices.get(choice)
                RequestGradeLevel.objects.get(description=choice_now)
                RequestGradeLevel.request_client=client
                grade_level_choices_list.append(choice_now)
            except:
                choice_now = choices.get(choice)
                RequestGradeLevel.objects.create(description=choice_now)
                RequestGradeLevel.request_client=client
                grade_level_choices_list.append(choice_now)
        data = {'message': 'success'}
        form = RequestInfoForm()
        notice_context ={'fullname':fullname, 'email':email, 'grade_level_choices':grade_level_choices_list, 'date': datetime.now()}
        body = render_to_string('mainpage/info_request_internal_notice.html', notice_context)
        pm_write(sender=self.sender, recipient=self.recipient, subject='Info request recieved', body=body, skip_notification=False,
        auto_archive=False, auto_delete=False, auto_moderators=None)
        message = 'Thank you for contacting us at EngageCS.  Your request for information has been receive and one of our staff members will contact you shortly.'
        recipient_list =[email]
        context = {'fullname': fullname}
        html_message = render_to_string('mainpage/info_request_reply.html', context)
        subject = 'Your request for info was recieved.'
        sendEmail.delay(subject, message, settings.DEFAULT_FROM_EMAIL, recipient_list, fail_silently=False, html_message=html_message)
        return JsonResponse(data)

    def form_invalid(self, form):
        result = super(RequestInfoFormView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        return result
