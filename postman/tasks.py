from __future__ import absolute_import

from celery import shared_task
import os
from engagecs.utils import send_email


@shared_task
def sendEmail(subject, message, from_email, recipient_list, **kwargs):

	send_email(subject, message, from_email, recipient_list, **kwargs)