from settings.common import *


DEBUG = False
THUMBNAIL_DEBUG = False
ALLOWED_HOSTS=['*']

BASE_URL = 'https://engagecs.com'

INSTALLED_APPS += ('storages',)

AWS_HEADERS = {  # see http://developer.yahoo.com/performance/rules.html#expires
        'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
        'Cache-Control': 'max-age=94608000',
    }

AWS_STORAGE_BUCKET_NAME = 'static-ecs'
AWS_MEDIA_BUCKET_NAME = 'media-ecs'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
S3_MEDIA_URL = 'https://%s.s3.amazonaws.com/' % AWS_MEDIA_BUCKET_NAME
STATIC_URL = S3_URL
MEDIA_URL = S3_MEDIA_URL 


DATABASES = {    
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ecs_db',
        'USER' : 'engagecs_db',
        'PASSWORD' : pwd,
        'HOST' : 'ecs-db.cboue0ezruns.us-west-2.rds.amazonaws.com',
        'PORT' : '5432',
    }
}
