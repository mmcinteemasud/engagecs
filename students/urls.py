from django.conf.urls import url
from . import views


urlpatterns = [

	url(r'^student-dashboard/$', views.StudentDashboardView.as_view(),
		name='student_dashboard'),
	url(r'^student-unit/(?P<pk>\d+)/$', views.StudentUnitDetailView.as_view(),
		name='student_unit_detail'),
	url(r'^student-unit/(?P<pk>\d+)/(?P<module_id>\d+)/$', views.StudentUnitDetailView.as_view(),
		name='student_unit_detail_module'),
	url(r'^assignment-submissions/(?P<pk>\d+)/$', views.StudentAssignmentSubmissionsView.as_view(),
		name='student_assignment_submissions'),
	]