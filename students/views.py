from django.shortcuts import render
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import authenticate, login
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin
from classes.models import Classroom, AssignmentSubmission
from accounts.models import Student
from units.models import Unit
from accounts.forms import JoinClassForm
# Create your views here.


class StudentDashboardView(LoginRequiredMixin, TemplateView):
	template_name = 'students/student_dashboard.html'
	def get(self, request, *args, **kwargs):
		student = Student.objects.get(user=request.user)
		classes = Classroom.objects.filter(students=student)
		join_class_form = JoinClassForm()
		context = self.get_context_data(**kwargs)
		context['join_class_form'] = join_class_form
		context['classes'] = classes
		return render(request, 'students/student_dashboard.html', context)



class StudentUnitDetailView(LoginRequiredMixin, DetailView):
	model = Unit
	template_name = 'students/unit/student_unit_detail.html'

	def get_context_data(self, **kwargs):
		context = super(StudentUnitDetailView, self).get_context_data(**kwargs)
		# get unit object
		unit = self.get_object()
		items = []
		# if classroom passed into url, return unit or module detail for units associated with that particular classroom
		if self.request.method == 'GET' and 'class' in self.request.GET:
			classroom_id = self.request.GET['class']
			classroom = Classroom.objects.get(id=classroom_id)
			if 'module_id' in self.kwargs:
				# get current module
				module = unit.modules.get(id=self.kwargs['module_id'])
				for content in module.contents.all():
					if 'restricted' not in str(content.content_type):
						items.append(content.item)
				context['classroom'] = classroom
				context['items'] = items
			else:
				# get first module
				module = unit.modules.all()[0]
				for content in module.contents.all():
					# restricted content is only for teachers
					if 'restricted' not in str(content.content_type):
						items.append(content.item)
				context['classroom'] = classroom
				context['items'] = items
		# if only unit and module id passed in url, go to student_unit_detail page with context for that module.
		elif 'module_id' in self.kwargs:
			# get current module
			module = unit.modules.get(id=self.kwargs['module_id'])
			for content in module.contents.all():
				if 'restricted' not in str(content.content_type):
					items.append(content.item)
			context['items'] = items
		# if only unit id passed in with no classroom passed in, go to student_unit_detail.html page with context for the unit.
		else:
			# get first module
			module = unit.modules.all()[0]
			for content in module.contents.all():
				if 'restricted' not in str(content.content_type):
					items.append(content.item)
			context['items'] = items
		return context

class StudentAssignmentSubmissionsView(LoginRequiredMixin, DetailView):
	model = Student
	template_name = 'students/student_assignment_submissions.html'

	def get_context_data(self, **kwargs):
		context = super(StudentAssignmentSubmissionsView, self).get_context_data(**kwargs)
		student = self.get_object()
		assignment_submission_list = AssignmentSubmission.objects.filter(student=student)
		context['assignment_submission_list'] = assignment_submission_list
		return context