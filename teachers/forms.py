from django import forms
from django.forms import ModelForm
from units.models import Unit
from classes.models import Classroom


class UnitEnrollForm(forms.Form):
	unit = forms.ModelChoiceField(queryset=Unit.objects.all(), widget=forms.HiddenInput)

class ClassCreateForm(ModelForm):
	
	class Meta():
		model = Classroom
		fields = ["title"]

	def __init__(self, units, *args, **kwargs):
		super(ClassCreateForm, self).__init__(*args, **kwargs)
		self.fields['add_units'] = forms.ModelMultipleChoiceField(
        	queryset=units,
        	widget=forms.CheckboxSelectMultiple,
            required=True,
        )
		self.fields["title"] = forms.CharField(
			label="Class Title",
            widget=forms.TextInput(attrs={'class':'form-control'})
            )



