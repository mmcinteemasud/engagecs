from django.conf.urls import url
from . import views


urlpatterns = [

	url(r'^enroll-unit/$', views.TeacherEnrollUnitView.as_view(),
		name='teacher_enroll_unit'),
	url(r'^dashboard/$', views.TeacherDashboardView.as_view(),
		name='teacher_dashboard'),
	url(r'^units/$', views.TeacherUnitListView.as_view(),
		name='teacher_unit_list'),
	url(r'^classrooms/$', views.TeacherClassroomListView.as_view(),
		name='teacher_classroom_list'),
	url(r'^unit/(?P<pk>\d+)/$', views.TeacherUnitDetailView.as_view(),
		name='teacher_unit_detail'),
	url(r'^unit/(?P<pk>\d+)/(?P<module_id>\d+)/$', views.TeacherUnitDetailView.as_view(),
		name='teacher_unit_detail_module'),

]
