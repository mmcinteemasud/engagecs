from django.shortcuts import render, render_to_response
from urllib import request
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin
from .forms import UnitEnrollForm, ClassCreateForm
from django.views.generic.list import ListView
from units.models import Unit, Grade, Subject, School
from accounts.models import Teacher
from classes.models import Classroom
from django.views.generic.detail import DetailView
from django.views.generic.base import View
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.
class TeacherEnrollUnitView(LoginRequiredMixin, FormView):
	unit = None
	form_class = UnitEnrollForm
	def form_valid(self, form):
		self.unit = form.cleaned_data['unit']
		self.unit.teachers.add(self.request.user)
		return super(TeacherEnrollUnitView, self).form_valid(form)

	def get_success_url(self):
		return reverse_lazy('teacher_unit_detail', args=[self.unit.id])


class TeacherDashboardView(LoginRequiredMixin, TemplateView):
	template_name = 'teachers/unit/teacher_dashboard.html'
	def get(self, request, *args, **kwargs):
		return render(request,'teachers/unit/teacher_dashboard.html')

class TeacherUnitListView(LoginRequiredMixin, TemplateView):
	template_name = 'teachers/unit/units.html'
	def get(self, request, *args, **kwargs):
		return render(request,'teachers/unit/units.html')

class TeacherClassroomListView(LoginRequiredMixin, TemplateView):
	template_name = 'teachers/unit/classes.html'
	def get(self, request, *args, **kwargs):
		teacher = Teacher.objects.get(user=self.request.user)
		units = teacher.units.all()
		class_create_form = ClassCreateForm(units=units)
		context = self.get_context_data(**kwargs)
		context['class_create_form'] = class_create_form
		return render(request,'teachers/unit/classes.html', context)


class TeacherUnitDetailView(DetailView):
	model = Unit
	template_name = 'teachers/unit/detail.html'

	def get_context_data(self, **kwargs):
		context = super(TeacherUnitDetailView, self).get_context_data(**kwargs)
		# get unit object
		unit = self.get_object()
		if self.request.method == 'GET' and 'class' in self.request.GET:
			classroom_id = self.request.GET['class']
			classroom = Classroom.objects.get(id=classroom_id)
			context['classroom'] = classroom
			if 'module_id' in self.kwargs:
				# get current module
				context['module'] = unit.modules.get(id=self.kwargs['module_id'])
				context['classroom'] = classroom
			else:
				# get first module
				context['module'] = unit.modules.all()[0]
				context['classroom'] = classroom
		elif 'module_id' in self.kwargs:
			# get current module
			context['module'] = unit.modules.get(id=self.kwargs['module_id'])
		else:
			# get first module
			context['module'] = unit.modules.all()[0]
		return context
