from django.contrib import admin

# Register your models here.

from django.contrib import admin
from .models import Subject, Grade, Unit, Module, School


@admin.register(School)
class SubjectAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug']
	prepopulated_fields = {'slug': ('name',)}

@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
	list_display = ['title', 'slug']
	prepopulated_fields = {'slug': ('title',)}

@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):
	list_display = ['title', 'slug']
	prepopulated_fields = {'slug': ('title',)}

class ModuleInline(admin.StackedInline):
	model = Module

@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
	list_display = ['title', 'subject', 'created']
	list_filter = ['created', 'subject', 'target_grades']
	search_fields = ['title', 'overview']
	prepopulated_fields = {'slug': ('title',)}
	inlines = [ModuleInline]
