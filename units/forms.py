from django import forms
from django.forms import ModelForm
from django.forms.models import inlineformset_factory
from django.forms import BaseInlineFormSet
from .models import Unit, Module, Grade


class BaseModuleFormSet(BaseInlineFormSet):
	def __init__(self, *args, **kwargs):
		super(BaseModuleFormSet, self).__init__(*args, **kwargs)

		for form in self.forms:
			for field in form.fields:
				form.fields[field].widget.attrs.update({'class': 'form-control static dirty'})

ModuleFormSet = inlineformset_factory(Unit, Module, fields=['title', 'description'], extra=2, can_delete=True, formset=BaseModuleFormSet)

class UnitBaseForm(ModelForm):

	class Meta:
		model = Unit
		fields = ['subject', 'title', 'overview', 'target_grades']

	def __init__(self, *args, **kwargs):
		super(UnitBaseForm, self).__init__(*args, **kwargs)
		self.fields['target_grades'] = forms.ModelMultipleChoiceField(
        	queryset=Grade.objects.all(),
        	widget=forms.CheckboxSelectMultiple,
            required=True,
        )
		self.fields['subject'].widget.attrs={
            'class': 'btn btn-default dropdown-toggle'}
		self.fields['title'].widget.attrs={
            'class': 'form-control static dirty'}
