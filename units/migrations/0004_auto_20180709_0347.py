# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-07-09 03:47
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('units', '0003_auto_20180514_2050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
