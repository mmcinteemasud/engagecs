from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from ckeditor_uploader.fields import RichTextUploadingField
from .fields import OrderField
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from django.conf import settings
# Create your models here.

class Grade(models.Model):
	title = models.PositiveIntegerField()
	slug = models.SlugField(max_length=200, unique=True)

	class Meta:
		ordering = ('title',)

	def __str__(self):
		return str(self.title)

class School(models.Model):
	name = models.CharField(max_length=200)
	address1 = models.CharField(max_length=200)
	address2 = models.CharField(max_length=200, blank=True)
	city = models.CharField(max_length=200)
	state = models.CharField(max_length=200, blank=True)
	zip = models.CharField(max_length=200, blank=True)
	country = models.CharField(max_length=200)
	phone = models.CharField(max_length=200, blank=True)
	slug = models.SlugField(max_length=200, unique=True)

	class Meta:
		ordering = ('name',)

	def __str__(self):
		return self.name

class Subject(models.Model):
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200, unique=True)

	class Meta:
		ordering = ('title',)

	def __str__(self):
		return self.title


class Unit(models.Model):
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='units_created')
	subject = models.ForeignKey(Subject, related_name='units')
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200, unique=True)
	overview = RichTextField(config_name='user_ckeditor', blank=True)
	created = models.DateTimeField(auto_now_add=True)
	target_grades = models.ManyToManyField(Grade, related_name='target_grades', blank=True)

	class Meta:
		ordering = ('title',)

	def _get_unique_slug(self):
		slug = slugify(self.title)
		unique_slug = slug
		num = 1
		while Unit.objects.filter(slug=unique_slug).exists():
			unique_slug = '{}-{}'.format(slug, num)
			num += 1
		return unique_slug

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = self._get_unique_slug()
		super().save(*args, **kwargs)

	def __str__(self):
		return self.title

class Module(models.Model):
	unit = models.ForeignKey(Unit, related_name='modules')
	title = models.CharField(max_length=200)
	description = RichTextField(config_name='user_ckeditor', blank=True)
	order = OrderField(blank=True, for_fields=['unit'])

	class Meta:
		ordering = ['order']

	def __str__(self):
		return '{}. {}'.format(self.order, self.title)


class Content(models.Model):
	module = models.ForeignKey(Module, related_name='contents')
	content_type = models.ForeignKey(ContentType, limit_choices_to={'model__in':('text', 'video', 'image', 'file', 'questions', 'restrictedText', 'restrictedVideo', 'restrictedImage', 'restrictedFile', 'restrictedAnswers')})
	object_id = models.PositiveIntegerField()
	item = GenericForeignKey('content_type', 'object_id')
	order = OrderField(blank=True, for_fields=['module'])

	class Meta:
		ordering = ['order']

class ItemBase(models.Model):
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='%(class)s_related')
	title = models.CharField(max_length=250)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

	def __str__(self):
		return self.title
	def render(self):
		return render_to_string('units/content/{}.html'.format(self._meta.model_name), {'item': self})

class Text(ItemBase):
	content = RichTextUploadingField(config_name='default')

class File(ItemBase):
	file = models.FileField(upload_to='files')

class Image(ItemBase):
	file = models.FileField(upload_to='images')

class Video(ItemBase):
	url = models.URLField()

class Questions(ItemBase):
	questions = RichTextUploadingField(config_name='default')


class RestrictedText(ItemBase):
	content = RichTextUploadingField(config_name='default')

class RestrictedFile(ItemBase):
	file = models.FileField(upload_to='files')

class RestrictedImage(ItemBase):
	file = models.FileField(upload_to='images')

class RestrictedVideo(ItemBase):
	url = models.URLField()

class RestrictedAnswers(ItemBase):
	answers = RichTextUploadingField(config_name='default')