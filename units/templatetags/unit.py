from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
def model_name(obj):
	try:
		name = obj._meta.model_name
		if name == 'restrictedtext':
			return 'restrictedText'
		elif name == 'restrictedimage':
			return 'restrictedImage'
		elif name == 'restrictedvideo':
			return 'restrictedVideo'
		elif name == 'restrictedanswers':
			return 'restrictedAnswers'
		elif name == 'restrictedfile':
			return 'restrictedFile'
		else:
			return obj._meta.model_name
	except AttributeError:
		return None

