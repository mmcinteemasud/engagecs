from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^mine/$', views.ManageUnitListView.as_view(), name='manage_unit_list'),
	url(r'^create/$', views.UnitCreateView.as_view(), name='unit_create'),
	url(r'^(?P<pk>\d+)/edit/$', views.UnitUpdateView.as_view(), name='unit_edit'), url(r'^(?P<pk>\d+)/delete/$',
	views.UnitDeleteView.as_view(), name='unit_delete'),
	url(r'^(?P<pk>\d+)/module/$', views.UnitModuleUpdateView.as_view(), name='unit_module_update'),
	url(r'^module/(?P<module_id>\d+)/content/(?P<model_name>\w+)/create/$',
		views.ContentCreateUpdateView.as_view(),
		name='module_content_create'),
	url(r'^module/(?P<module_id>\d+)/content/(?P<model_name>\w+)/(?P<id>\d+)/$',
		views.ContentCreateUpdateView.as_view(), name='module_content_update'),
	url(r'^content/(?P<id>\d+)/delete/$', views.ContentDeleteView.as_view(),
		name='module_content_delete'),
	url(r'^module/(?P<module_id>\d+)/$', views.ModuleContentListView.as_view(),
		name='module_content_list'),
	url(r'^module/order/$', views.ModuleOrderView.as_view(),
		name='module_order'),
	url(r'^content/order/$', views.ContentOrderView.as_view(),
		name='content_order'),
	url(r'^subject/(?P<subject>[\w-]+)/$', views.UnitListView.as_view(),
		name='unit_list_subject'),
	url(r'^(?P<slug>[\w-]+)/$', views.UnitDetailView.as_view(),
		name='unit_detail'),


]
