from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from braces.views import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import redirect, get_object_or_404
from django.views.generic.base import TemplateResponseMixin, View
from .forms import ModuleFormSet, UnitBaseForm
from django.forms.models import modelform_factory
from django.apps import apps
from .models import Module, Content
from accounts.models import Teacher
from braces.views import CsrfExemptMixin, JsonRequestResponseMixin
from django.db.models import Count
from .models import Subject
from .models import Unit
from django.views.generic.detail import DetailView
from teachers.forms import UnitEnrollForm
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

class OwnerMixin(object):
	def get_queryset(self):
		qs = super(OwnerMixin, self).get_queryset()
		return qs.filter(owner=self.request.user)

class OwnerEditMixin(object):
	def form_valid(self, form):
		form.instance.owner = self.request.user
		return super(OwnerEditMixin, self).form_valid(form)

class OwnerUnitMixin(OwnerMixin, LoginRequiredMixin):
	model = Unit
	form_class = UnitBaseForm
	success_url = reverse_lazy('manage_unit_list')


class OwnerUnitEditMixin(OwnerUnitMixin, OwnerEditMixin):
	form_class = UnitBaseForm
	success_url = reverse_lazy('manage_unit_list')
	template_name = 'units/manage/unit/form.html'

class ManageUnitListView(OwnerUnitMixin, ListView):
	template_name = 'units/manage/unit/list.html'

class UnitCreateView(PermissionRequiredMixin, OwnerUnitEditMixin, CreateView):
	permission_required = 'units.add_unit'
	success_url = reverse_lazy('manage_unit_list')

	def form_valid(self, form):
		form.instance.owner = self.request.user
		unit = form.save()
		grades = unit.target_grades.all()
		subject = unit.subject
		# find all teachers that have access to the subject and target_grade of this newly created unit and add the unit to the teachers.units.  If no
		# teachers are found, move on
		try:
			teachers = Teacher.objects.filter(subjects_taught__in=[subject]).filter(grades_taught__in=[grade for grade in grades]).distinct()
			for teacher in teachers:
				teacher.units.add(unit)
			return super(UnitCreateView, self).form_valid(form)
		except:
			return super(UnitCreateView, self).form_valid(form)

class UnitUpdateView(PermissionRequiredMixin, OwnerUnitEditMixin, UpdateView):
	permission_required = 'units.change_unit'


class UnitDeleteView(PermissionRequiredMixin, OwnerUnitMixin, DeleteView):
	template_name = 'units/manage/unit/delete.html'
	success_url = reverse_lazy('manage_unit_list')
	permission_required = 'units.delete_unit'

class UnitModuleUpdateView(TemplateResponseMixin, View):
	template_name = 'units/manage/module/formset.html'
	unit = None

	def get_formset(self, data=None):
		return ModuleFormSet(instance=self.unit, data=data)

	def dispatch(self, request, pk):
		self.unit = get_object_or_404(Unit, id=pk, owner=request.user)
		return super(UnitModuleUpdateView, self).dispatch(request, pk)

	def get(self, request, *args, **kwargs):
		formset = self.get_formset()
		return self.render_to_response({'unit': self.unit, 'formset': formset})
	
	def post(self, request, *args, **kwargs):
		formset = self.get_formset(data=request.POST)
		if formset.is_valid():
			formset.save()
			return redirect('manage_unit_list')
		return self.render_to_response({'unit': self.unit, 'formset': formset})


class ContentCreateUpdateView(TemplateResponseMixin, View):
	module = None
	model = None
	obj = None
	template_name = 'units/manage/content/form.html'


	def get_model(self, model_name):
		if model_name in ['text', 'video', 'image', 'file', 'questions', 'restrictedText', 'restrictedVideo', 'restrictedImage', 'restrictedFile', 'restrictedAnswers']:
			return apps.get_model(app_label='units', model_name=model_name)
		return None

	def get_form(self, model, *args, **kwargs):
		Form = modelform_factory(model, exclude=['owner', 'order', 'created', 'updated'])
		return Form(*args, **kwargs)

	def dispatch(self, request, module_id, model_name, id=None): 
		self.module = get_object_or_404(Module, id=module_id, unit__owner=request.user)
		self.model = self.get_model(model_name)
		if id:
			self.obj = get_object_or_404(self.model, id=id, owner=request.user)
		return super(ContentCreateUpdateView, self).dispatch(request, module_id, model_name, id)

	def get(self, request, module_id, model_name, id=None):
		module = get_object_or_404(Module, id=module_id, unit__owner=request.user)
		form = self.get_form(self.model, instance=self.obj)
		return self.render_to_response({'form': form, 'object': self.obj, 'module': module, 'model_name':model_name})

	def post(self, request, module_id, model_name, id=None):
		form = self.get_form(self.model, instance=self.obj, data=request.POST, files=request.FILES)

		if form.is_valid():
			print('form valid')
			obj = form.save(commit=False)
			obj.owner = request.user
			obj.save()
			if not id:
				print('new content')
				# new content
				Content.objects.create(module=self.module, item=obj)
			return redirect('module_content_list', self.module.id)
		return self.render_to_response({'form': form, 'object': self.obj})

class ContentDeleteView(View):
	def post(self, request, id):
		content = get_object_or_404(Content,
			id=id,
			module__unit__owner=request.user)
		module = content.module
		content.item.delete()
		content.delete()
		return redirect('module_content_list', module.id)

class ModuleContentListView(TemplateResponseMixin, View):
	template_name = 'units/manage/module/content_list.html'
	def get(self, request, module_id):
		module = get_object_or_404(Module,
			id=module_id,
			unit__owner=request.user)
		return self.render_to_response({'module': module})

class ModuleOrderView(CsrfExemptMixin, JsonRequestResponseMixin, View):
	def post(self, request):
		for id, order in self.request_json.items():
			Module.objects.filter(id=id, unit__owner=request.user).update(order=order)	
		return self.render_json_response({'saved': 'OK'})

class ContentOrderView(CsrfExemptMixin, JsonRequestResponseMixin, View):
	def post(self, request):
		for id, order in self.request_json.items():
			Content.objects.filter(id=id, module__unit__owner=request.user) \
				.update(order=order)
		return self.render_json_response({'saved': 'OK'})

class UnitListView(TemplateResponseMixin, View):
	model = Unit
	template_name = 'units/unit/list.html'
	def get(self, request, subject=None):
		subjects = Subject.objects.annotate(total_units=Count('units'))
		units = Unit.objects.annotate(total_modules=Count('modules'))
		if subject:
			subject = get_object_or_404(Subject, slug=subject)
			units = units.filter(subject=subject)
		return self.render_to_response({'subjects': subjects, 'subject': subject, 'units': units})

class UnitDetailView(DetailView):
	model = Unit
	template_name = 'units/unit/detail.html'

	def get_context_data(self, **kwargs):
		context = super(UnitDetailView, self).get_context_data(**kwargs)
		context['enroll_form'] = UnitEnrollForm(initial={'unit':self.object})
		return context
